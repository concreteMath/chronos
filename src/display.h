#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

/*
// Screen width
#include <sys/ioctl.h>
*/

typedef struct display {
	int n_digits;
	int* digitsNew;
	int* digitsOld;
	int* digitsPos;
	//TODO Este parâmetro poderá a vir dar jeito se tentar centrar o relógio
	//int32_t screen_width;
} display_t;

int8_t display_init(display_t* d, int n_digits);
int8_t display_free(display_t* d);
int8_t display_time_update(display_t* d, int64_t tNanoNew);

int8_t display_setDigitsPos(display_t* d);
void display_nano2digits(int* digits, int64_t tNano);
void display_big_clear();
void display_big_zeros(display_t* disp);
void display_big(display_t* disp);
