#include <stdio.h>
#include <time.h>
#include <inttypes.h>

//TERMINAL
#include <stdlib.h>
#include <termios.h>
#include <string.h>

//KEYBOARD
#include <unistd.h>


// TERMINAL
struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}


// KEYBOARD
// Verifica se existe input no stream do teclado (acho eu)
int kbhit()
{
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

// Devolve o caracter que está no stream (acho eu)
int getch()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}




//FIXME O ideal seria fazer print apenas dos digitos que mudam
//      Para isso, teria de calcular quais seriam esses dígitos
// Print da hora em milisegundos
void printTime(int64_t tNano)
{
	int64_t ms, s, m, h;

	ms = tNano/1000000;

	s   = ms/1000;
	ms -= s*1000;

	m  = s/60;
	s -= m*60;

	h  = m/60;
	m -= h*60;
	
	printf("\e[G%02ld:%02ld:%02ld:%03ld", h, m, s, ms);
	fflush(stdout);
}

// Definição
const int64_t GIGA = 1000000000;

int main ()
{

	// TERMINAL
	set_conio_terminal_mode();
	
	// KEYBOARD
	// keyboard input variable
	char c=0;
	
	// LOGIC
	// If this flag is active, the counting resumes
	int8_t f_count = 1;

	// CHRONOMETER
	int64_t t0, tReal, tSleep;
	struct timespec tstart={0,0}, tend={0,0}, dt={0,0};
	
	// CICLES
	// Cicle counter
	int64_t i = 0;
	// Cicle duration
	const int64_t tCicle = GIGA/10;
	
	// First Counting
	clock_gettime(CLOCK_MONOTONIC, &tstart);
	t0 = tstart.tv_sec * GIGA + tstart.tv_nsec; 

	while (1) {
		while (!kbhit()) {

			if (f_count)
				i++;
			
			// Quando entro dentro do ciclo, o que o programa quer saber é
			// quanto tempo tem de esperar até ao início do próximo ciclo.
			// A varíavel que vai dar o tempo para o mostrador é o o contador
			// de ciclos.

			clock_gettime(CLOCK_MONOTONIC, &tend);
			tReal = tend.tv_sec*GIGA + tend.tv_nsec;

			tSleep = tCicle - ((tReal-t0)%tCicle);
			dt.tv_sec  = tSleep/GIGA;
			dt.tv_nsec = tSleep%GIGA;

			nanosleep(&dt, NULL);

			if (f_count)
				printTime(i*tCicle);

		}
		
		// get input from keyboard
		c = getch();

		//LOGIC
		switch(c) {
		case 3:
			// ctrl+c
			printf("\e[G\n");
			return 0;
		case 32:
			// espaço
			f_count = !f_count;
			break;
		}

	}

	printf("\e[G\n");
	return 0;
}
