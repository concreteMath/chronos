#include <stdio.h>
#include <time.h>
#include <inttypes.h>

//TERMINAL
#include <stdlib.h>
#include <termios.h>
#include <string.h>

//KEYBOARD
#include <unistd.h>

//DISPLAY
#include "display.h"


// TERMINAL
struct termios orig_termios;

void reset_terminal_mode()
{
    tcsetattr(0, TCSANOW, &orig_termios);
}

void set_conio_terminal_mode()
{
    struct termios new_termios;

    /* take two copies - one for now, one for later */
    tcgetattr(0, &orig_termios);
    memcpy(&new_termios, &orig_termios, sizeof(new_termios));

    /* register cleanup handler, and set the new terminal mode */
    atexit(reset_terminal_mode);
    cfmakeraw(&new_termios);
    tcsetattr(0, TCSANOW, &new_termios);
}


// KEYBOARD
// Verifica se existe input no stream do teclado (acho eu)
int kbhit()
{
    struct timeval tv = { 0L, 0L };
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(0, &fds);
    return select(1, &fds, NULL, NULL, &tv);
}

// Devolve o caracter que está no stream (acho eu)
int getch()
{
    int r;
    unsigned char c;
    if ((r = read(0, &c, sizeof(c))) < 0) {
        return r;
    } else {
        return c;
    }
}




//FIXME O ideal seria fazer print apenas dos digitos que mudam
//      Para isso, teria de calcular quais seriam esses dígitos
// Print da hora em milisegundos
void printTime(int64_t tNano)
{
	int64_t cs, s, m, h;

	cs = tNano/10000000;

	s   = cs/100;
	cs -= s*100;

	m  = s/60;
	s -= m*60;

	h  = m/60;
	m -= h*60;
	
	printf("\e[G%02ld:%02ld:%02ld.%02ld", h, m, s, cs);
	fflush(stdout);
}

// Definição
const int64_t GIGA = 1000000000;

int main ()
{
	// USAGE FLAGS
	//FIXME esta flag não deveria ser variável.
	const int f_sync = 1;
	const int f_disp = 1;
	

	// TERMINAL
	set_conio_terminal_mode();

	// HIDE CURSOR
	printf("\e[?25l");
	
	// CONFIGURE DISPLAY
	display_t disp;
	display_init(&disp, 6);
	display_setDigitsPos(&disp);
	display_big_zeros(&disp);
	
	// KEYBOARD
	// keyboard input variable
	char c=0;
	
	// LOGIC
	// This flag is active during the chronometer's cicle counting.
	int8_t f_count = 1;
	// This flag activates the synchronism functionality.
	int8_t f_sync_cicle = 0;

	// CHRONOMETER
	int64_t tReal, tNext, tSleep;
	struct timespec t_now={0,0}, dt={0,0};
	
	// CICLES
	// Cicle counter
	// all cicles
	int64_t i_all = 0;
	// next cicle
	int64_t i_next = 0;
	// running cicles
	int64_t i_run = 0;
	// Cicle duration
	//TODO: O tempo de ciclo deverá ser configurável, conforme a precisão do
	//      do cronómetro (ex: 1s, 0.1s, 0.01s).
	//const int64_t tCicle = GIGA/100;
	const int64_t tCicle = GIGA/10;
	
	// CICLE CORRECTION
	int64_t j = 0;
	const int64_t n_cicles_sync = 100;
	

	// Reference time
	clock_gettime(CLOCK_MONOTONIC, &t_now);
	const int64_t t0 = t_now.tv_sec * GIGA + t_now.tv_nsec; 
	
	while (1) {
		while (!kbhit()) {

			// PRINT DO TEMPO
			if (f_disp && f_count) {
				//printTime(i_run*tCicle);

				display_time_update(&disp, i_run*tCicle);
				display_big(&disp);
			}


			// CONTAGENS DE CICLOS

			// Determinação do ciclo de sincronização
			if (!j) {
				f_sync_cicle=1;
				j=n_cicles_sync;
			}
			j--;
			
			// Avanço dos contadores de ciclos
			i_all++;
			if (f_count)
				i_run++;


			// CÁLCULO DO TEMPO DE SLEEP
			
			// Quando entro dentro do ciclo, o que o programa quer saber é
			// quanto tempo vai ter de esperar até ao início do próximo ciclo.
			// A varíavel que vai dar o tempo para o mostrador é o contador
			// de ciclos.

			// Este bloco garante-me contagem do tempo, mesmo com o computador
			// desligado.
			if (f_sync && f_sync_cicle) {
				// Reset da flag do ciclo de sincronização
				f_sync_cicle = 0;

				clock_gettime(CLOCK_MONOTONIC, &t_now);
				tReal = t_now.tv_sec*GIGA + t_now.tv_nsec;
				
				// Este garantidamente é o próximo tempo. (desde que não mexam
				// no relógio do computador)
				i_next = (tReal - t0)/tCicle + 1;
				tNext = i_next * tCicle + t0;
				
				// Actualização de i_all e i_run, caso i_all esteja atrasado.
				if (i_next > i_all) {
					if (f_count)
						i_run += (i_next - i_all);
					i_all = i_next;
				}

				tSleep = tNext - tReal;
			} else {
				tSleep = tCicle;
			}
			
			dt.tv_sec  = tSleep/GIGA;
			dt.tv_nsec = tSleep%GIGA;


			// SLEEP

			nanosleep(&dt, NULL);
		}
		
		// get input from keyboard
		c = getch();

		//LOGIC
		switch(c) {
		case 3:
			// ctrl+c
			display_free(&disp);
			printf("\e[G\e[5B");
			// SHOW CURSOR
			printf("\e[?25h");
			// Reset terminal
			reset_terminal_mode();
			// Print elapsed time
			printf("\n");
			printf("%f\n", (double)(i_run*tCicle)/(double)GIGA);
			return 0;
		case 32:
			// espaço
			f_count = !f_count;
			break;
		}

	}

	return 0;
}
