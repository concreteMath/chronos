#include "display.h"

void display_nano2digits(int* digits, int64_t tNano)
{
	int64_t s, m, h;

	s = tNano/1000000000;

	m  = s/60;
	s -= m*60;

	h  = m/60;
	m -= h*60;

	digits[0] = h/10;
	digits[1] = h%10;

	digits[2] = m/10;
	digits[3] = m%10;

	digits[4] = s/10;
	digits[5] = s%10;

}

/*
int32_t get_screen_width()
{
	struct winsize ws;
	ioctl(0, TIOCGWINSZ, &ws);
	return ws.ws_col;
}
*/


int8_t display_init(display_t* d, int n_digits)
{
	d->n_digits = n_digits;
	d->digitsNew = calloc(n_digits, sizeof(int));
	d->digitsOld = calloc(n_digits, sizeof(int));
	d->digitsPos = calloc(n_digits, sizeof(int));
	//d->screen_width = -1;
	return 0;
}


int8_t display_free(display_t* d)
{
	free(d->digitsNew);
	free(d->digitsOld);
	free(d->digitsPos);
	return 0;
}


int8_t display_setDigitsPos(display_t* d)
{
	const int32_t dPos0[] = {2, 9, 18, 25, 34, 41};
	
	/*
	int32_t width  = get_screen_width();

	if (width == d->screen_width) {
		return 0;
	} else {
		d->screen_width = width;
	}
	
	// 46char = 6digits * 7char/digit + 2sep * 2char/sep
	int32_t margin = (width - 46)/2;
	if (margin < 0)
		margin = 0;
	*/

	for (int i=0; i<d->n_digits; i++) {
		//*(d->digitsPos + i) = margin + dPos0[i];
		*(d->digitsPos + i) = dPos0[i];
	}
	return 1;
}


int8_t display_time_update(display_t* d, int64_t tNanoNew)
{
	// Cópia dos dígitos antigos
	for (int i=0; i<d->n_digits; i++) {
		*(d->digitsOld + i) = *(d->digitsNew + i);
	}
	
	// Novos dígitos
	display_nano2digits(d->digitsNew, tNanoNew);

	return 0;
}

void display_big_clear()
{
	printf("\e[2J");
	//for (int i=0; i<5; i++) {
	//	printf("\e[2K\e[1B");
	//}
	//printf("\e[5A");
}

void display_big_zeros(display_t* disp)
{
	const int H = 5;

	const char* char_big[] = {
	"██████",
	"██  ██",
	"██  ██",
	"██  ██",
	"██████"};

	const char* sep[] = {
	" ",
	"█",
	" ",
	"█",
	" "};

	//printf("\n");
	for (int i=0; i<H; i++) {
		printf("\e[%dG%s", (disp->digitsPos)[0], char_big[i]);
		printf("\e[%dG%s", (disp->digitsPos)[1], char_big[i]);

		printf("\e[%dG%s", (disp->digitsPos)[1]+7, sep[i]);

		printf("\e[%dG%s", (disp->digitsPos)[2], char_big[i]);
		printf("\e[%dG%s", (disp->digitsPos)[3], char_big[i]);

		printf("\e[%dG%s", (disp->digitsPos)[3]+7, sep[i]);

		printf("\e[%dG%s", (disp->digitsPos)[4], char_big[i]);
		printf("\e[%dG%s", (disp->digitsPos)[5], char_big[i]);

		printf("\e[G\n");
	}
	printf("\e[5A");
}

void display_big(display_t* disp)
{
	const int H = 5;
	
	/*
	const char* char_big[] = {
	"█████","    █","█████","█████","█   █","█████","█████","█████","█████","█████",
	"█   █","    █","    █","    █","█   █","█    ","█    ","    █","█   █","█   █",
	"█   █","    █","█████","█████","█████","█████","█████","    █","█████","█████",
	"█   █","    █","█    ","    █","    █","    █","█   █","    █","█   █","    █",
	"█████","    █","█████","█████","    █","█████","█████","    █","█████","█████"};
	*/
	
	const char* char_big[] = {
	"██████","    ██","██████","██████","██  ██","██████","██████","██████","██████","██████",
	"██  ██","    ██","    ██","    ██","██  ██","██    ","██    ","    ██","██  ██","██  ██",
	"██  ██","    ██","██████","██████","██████","██████","██████","    ██","██████","██████",
	"██  ██","    ██","██    ","    ██","    ██","    ██","██  ██","    ██","██  ██","    ██",
	"██████","    ██","██████","██████","    ██","██████","██████","    ██","██████","██████"};

	/*
	const char* sep[] = {
	" ",
	"█",
	" ",
	"█",
	" "};
	*/

	int c0;

	for (int i=0; i<H; i++) {
		c0 = i*10;

		for (int j=0; j<6; j++) {
			if ((disp->digitsNew)[j] != (disp->digitsOld)[j])
				printf("\e[%dG%s", (disp->digitsPos)[j],
				       char_big[c0 + (disp->digitsNew)[j]]);
		}

		printf("\e[G\n");
	}
	printf("\e[5A");
}
